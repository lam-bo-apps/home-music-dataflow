# Home music dataflow (WIP)


Contains Spring Batch tasks compiled in native mode with GraalVM, following a DDD code architecture.
These tasks are used to back up Spotify library data to Firebase Datastore and synchronize liked tracks to YouTube

## Important notice

This project has migrated to Github see: 

https://github.com/lam-bo-apps/home-music-dataflow for up-to-date project
https://github.com/lam-bo-apps/home-music-infra for Terraform deployment scripts

## Quickstart

See [QUICKSTART.md](QUICKSTART.md) to launch a simple task to log top tracks for a Spotify User